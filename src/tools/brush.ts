import ToolService from '../services/toolService';

export interface CursorOffsets extends EventTarget {
  offsetLeft: number,
  offsetTop: number,
}

export default class Brush extends ToolService {
  canvas: HTMLCanvasElement | null = null;

  mouseDown = false;

  constructor(canvas: HTMLCanvasElement | null) {
    super(canvas);

    this.canvas = canvas;
    this.listen();
  }

  listen() {
    if (this.canvas) {
      this.canvas.onmousedown = this.mouseDownHandler.bind(this);
      this.canvas.onmouseup = this.mouseUpHandler.bind(this);
      this.canvas.onmousemove = this.mouseMoveHandler.bind(this);
    }
  }

  mouseUpHandler(e: MouseEvent) {
    this.mouseDown = false;
  }

  mouseDownHandler(e: MouseEvent) {
    const eventTarget = e.target as CursorOffsets;
    if (e && e.target) {
      this.mouseDown = true;
      this.ctx?.beginPath();
      this.ctx?.moveTo(e.pageX - eventTarget.offsetLeft, e.pageY - eventTarget.offsetTop);
    }
  }

  mouseMoveHandler(e: MouseEvent) {
    if (this.mouseDown && (e && e.target)) {
      const eventTarget = e.target as CursorOffsets;
      this.draw(e.pageX - eventTarget.offsetLeft, e.pageY - eventTarget.offsetTop);
    }
  }

  draw(x: number, y: number) {
    this.ctx?.lineTo(x, y);
    this.ctx?.stroke();
  }
}
