import ToolService, { Canvas } from '../services/toolService';
import { CursorOffsets } from './brush';

export default class Rect extends ToolService {
  canvas: Canvas = null;

  mouseDown = false;

  startX = 0;

  startY = 0;

  saved = '';

  constructor(canvas: Canvas) {
    super(canvas);

    this.canvas = canvas;
    this.listen();
  }

  listen() {
    if (this.canvas) {
      this.canvas.onmousedown = this.mouseDownHandler.bind(this);
      this.canvas.onmouseup = this.mouseUpHandler.bind(this);
      this.canvas.onmousemove = this.mouseMoveHandler.bind(this);
    }
  }

  mouseUpHandler(e: MouseEvent) {
    this.mouseDown = false;
  }

  mouseDownHandler(e: MouseEvent) {
    const eventTarget = e.target as CursorOffsets;
    if (e && e.target && this.canvas) {
      this.mouseDown = true;
      this.ctx?.beginPath();
      this.startX = e.pageX - eventTarget.offsetLeft;
      this.startY = e.pageY - eventTarget.offsetTop;
      this.saved = this.canvas.toDataURL();
    }
  }

  mouseMoveHandler(e: MouseEvent) {
    if (this.mouseDown && (e && e.target)) {
      const eventTarget = e.target as CursorOffsets;
      const currentX = e.pageX - eventTarget.offsetLeft;
      const currentY = e.pageY - eventTarget.offsetTop;
      const width = currentX - this.startX;
      const height = currentY - this.startY;

      this.draw(this.startX, this.startY, width, height);
    }
  }

  draw(x: number, y: number, w: number, h: number) {
    const img = new Image();
    img.src = this.saved;

    img.onload = () => {
      if (this.canvas) {
        this.ctx?.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.ctx?.drawImage(img, 0, 0, this.canvas.width, this.canvas.height);
      }

      this.ctx?.beginPath();
      this.ctx?.rect(x, y, w, h);
      this.ctx?.fill();
      this.ctx?.stroke();
    };
  }
}
