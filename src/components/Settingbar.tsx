import React from 'react';
import toolState from '../store/toolState';

const SettingBar = () => (
  <div className="toolbar toolbar_top-40 toolbar_z-index-1">
    <label htmlFor="line-width">
      <input
        className="toolbar__size-input"
        type="number"
        id="line-width"
        defaultValue={1}
        min={1}
        max={50}
        onChange={(e) => toolState.setLineWidth(e.target.value)}
      />
      Line Width
    </label>
    <label htmlFor="line-width">
      <input
        id="stroke-color"
        type="color"
        className="toolbar__color-picker"
        onChange={(e) => toolState.setStrokeColor(e.target.value)}
      />
      Line Width
    </label>
  </div>
);

export default SettingBar;
