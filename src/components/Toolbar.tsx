import React, { ChangeEvent } from 'react';

import toolState from '../store/toolState';
import canvasState from '../store/canvasState';

import Brush from '../tools/brush';
import Rect from '../tools/rect';

const Toolbar = () => {
  const handleChangeColor = (e: ChangeEvent<HTMLInputElement>) => {
    toolState.setStrokeColor(e.target.value);
    toolState.setFillColor(e.target.value);
  };

  return (
    <div className="toolbar toolbar_z-index-2">
      <button
        type="button"
        aria-label="brush"
        className="toolbar__btn toolbar__btn_brush"
        onClick={() => toolState.setTool(new Brush(canvasState.canvas))}
      />
      <button
        type="button"
        aria-label="rect"
        className="toolbar__btn toolbar__btn_brush"
        onClick={() => toolState.setTool(new Rect(canvasState.canvas))}
      />
      <button
        type="button"
        aria-label="rect"
        className="toolbar__btn toolbar__btn_brush"
        onClick={() => canvasState.undo()}
      >
        Undo
      </button>
      <button
        type="button"
        aria-label="rect"
        className="toolbar__btn toolbar__btn_brush"
        onClick={() => canvasState.redo()}
      >
        Redo
      </button>
      <input type="color" onChange={handleChangeColor} className="toolbar__color-picker" />
    </div>
  );
};

export default Toolbar;
