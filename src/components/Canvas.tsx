import React, { useEffect, useRef } from 'react';
import { observer } from 'mobx-react-lite';
import canvasState from '../store/canvasState';
import toolState from '../store/toolState';
import Brush from '../tools/brush';
import { Canvas as CanvasType } from '../services/toolService';

const Canvas = observer(() => {
  const canvas = useRef<CanvasType>(null);

  useEffect(() => {
    if (canvas && canvas.current) canvasState.setCanvas(canvas.current);
    toolState.setTool(new Brush(canvas.current));
  });

  const mouseDownHandler = () => {
    if (canvas.current) canvasState.pushToUndo(canvas.current.toDataURL());
  };

  return (
    <div className="canvas">
      <canvas onMouseDown={mouseDownHandler} ref={canvas} width={600} height={400} />
    </div>
  );
});

export default Canvas;
