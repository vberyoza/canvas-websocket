export type Canvas = HTMLCanvasElement | null;

export default class ToolService {
  canvas: Canvas = null;

  ctx: CanvasRenderingContext2D | null = null;

  fillStyle = '';

  strokeStyle = '';

  constructor(canvas: Canvas) {
    this.canvas = canvas;
    this.ctx = canvas ? canvas.getContext('2d') : null;
    this.destroyEvents();
  }

  set fillColor(color: string) {
    if (this.ctx) this.ctx.fillStyle = color;
  }

  set strokeColor(color: string) {
    if (this.ctx) this.ctx.strokeStyle = color;
  }

  set lineWidth(width: number) {
    if (this.ctx) this.ctx.lineWidth = width;
  }

  destroyEvents() {
    if (this.canvas) {
      this.canvas.onmousemove = null;
      this.canvas.onmouseup = null;
      this.canvas.onmousemove = null;
    }
  }
}
