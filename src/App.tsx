import React from 'react';
import './styles/index.scss';

import Toolbar from './components/Toolbar';
import SettingBar from './components/Settingbar';
import Canvas from './components/Canvas';

function App() {
  return (
    <div className="App">
      <Toolbar />
      <SettingBar />
      <Canvas />
    </div>
  );
}

export default App;
