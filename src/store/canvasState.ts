import { makeAutoObservable } from 'mobx';
import { Canvas } from '../services/toolService';

class CanvasState {
  canvas: Canvas = null;

  redoList: string[] = [];

  undoList: string[] = [];

  constructor() {
    makeAutoObservable(this);
  }

  setCanvas(canvas: Canvas) {
    this.canvas = canvas;
  }

  pushToUndo(data: string) {
    this.undoList.push(data);
  }

  pushToRedo(data: string) {
    this.redoList.push(data);
  }

  historyImageOnLoad(ctx: CanvasRenderingContext2D, dataUrl?: string) {
    const img = new Image();
    img.src = dataUrl || '';

    img.onload = () => {
      if (this.canvas) {
        ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        ctx.drawImage(img, 0, 0, this.canvas.width, this.canvas.height);
      }
    };
  }

  undo() {
    const ctx = this.canvas?.getContext('2d');

    if (this.undoList.length && this.canvas && ctx) {
      const dataUrl = this.undoList.pop();
      this.pushToRedo(this.canvas.toDataURL());
      this.historyImageOnLoad(ctx, dataUrl);
    }

    if (!this.undoList.length && this.canvas && ctx) ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
  }

  redo() {
    const ctx = this.canvas?.getContext('2d');

    if (this.redoList.length && this.canvas && ctx) {
      const dataUrl = this.redoList.pop();
      this.pushToUndo(this.canvas.toDataURL());
      this.historyImageOnLoad(ctx, dataUrl);
    }

    if (!this.undoList.length && this.canvas && ctx) ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
  }
}

export default new CanvasState();
