import { makeAutoObservable } from 'mobx';
import Brush from '../tools/brush';
import Rect from '../tools/rect';

export type ToolType = Brush | Rect | null;

class ToolState {
  tool: ToolType = null;

  constructor() {
    makeAutoObservable(this);
  }

  setTool(tool: ToolType) {
    this.tool = tool;
  }

  setFillColor(color: string) {
    if (this.tool) {
      this.tool.fillColor = color;
    }
  }

  setStrokeColor(color: string) {
    if (this.tool) this.tool.strokeColor = color;
  }

  setLineWidth(width: string) {
    if (this.tool) this.tool.lineWidth = Number(width);
  }
}

export default new ToolState();
